import java.util.Calendar;

public class Book extends LibraryItem implements Borrowable {

    private String author;
    private String publisher;
    private String genre;
    private boolean borrowed;
    private double deweyDecimal;

    public Book(int id, String title, String releaseDate, String author, String publisher, String genre, double deweydecimal) {
        super.setTitle(title);
        super.setReleaseDate(releaseDate);
        super.setId(id);
        this.author = author;
        this.publisher = publisher;
        this.genre = genre;
        this.deweyDecimal = deweydecimal;
    }

    @Override
    public boolean borrowItem() {
        borrowed = true;
        return true;
    }

    @Override
    public boolean returnItem() {
        borrowed = false;
        return true;
    }

    @Override
    public boolean isBorrowed() {
        return this.borrowed;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getDeweydecimal() {
        return deweyDecimal;
    }

    public void setDeweydecimal(double deweydecimal) {
        this.deweyDecimal = deweydecimal;
    }
}
