import java.util.Calendar;

public abstract class LibraryItem {

    private int id;
    private String title;
    private String releaseDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public abstract boolean isBorrowed();

    public abstract boolean borrowItem();

    public abstract boolean returnItem();
}
