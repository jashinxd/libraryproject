public class Car {
    
    private String make;
    private String model;

    // Right click --> Source Action --> Make getters and setters
    // Getters and Setter adhere to the Java Beans conventions
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    
}