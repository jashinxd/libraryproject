public class Account2 {
	
	private String name;
	private double balance;
	private Currency currency;

	public Account2 (String name, double balance, Currency currency) throws DodgyNameException {
		if (name.equals("Fingers")) {
			throw new DodgyNameException();
		}
		this.name = name;
		this.balance = balance;
		this.currency = currency;
	}

	public double getBalance() {
		return balance;
	}
	public void setBalance(double b) {
		balance = b;
	}
	public String getName() {
		return name;
	}

	public void setName(String n) throws DodgyNameException {
		if (n.equals("Fingers")) {
			throw new DodgyNameException(); 
		}
		name = n;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}	
}
