public interface Borrowable {

    public boolean borrowItem();
    public boolean returnItem();

}
