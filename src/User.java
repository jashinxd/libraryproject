import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {

    private int id;
    private String name;
    private String email;
    private String dob;
    private Map<Integer, LibraryItem> borrowedItems;

    public User(int id, String name, String email, String dob) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dob = dob;
        borrowedItems = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public LibraryItem getBorrowedItem(int id) {
        return this.borrowedItems.get(id);
    }

    public boolean borrowItem(LibraryItem li) {
        this.borrowedItems.put(li.getId(), li);
        return true;
    }

    public boolean returnItem(int id) {
        this.borrowedItems.remove(id);
        return true;
    }

    public void printBorrowedItems() {
        System.out.println("" + name + "'s borrowed items:" );
        for (int id : borrowedItems.keySet()) {
            LibraryItem li = borrowedItems.get(id);
            System.out.println("" + li.getId() + "\t" + li.getTitle() + "\t" + li.isBorrowed());
        }
    }
}
