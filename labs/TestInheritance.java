public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = {new SavingsAccount("NewSavings", 2.0), new SavingsAccount("Savings", 4.0), new CurrentAccount("Current", 6.0)};
        for (Account a : accounts) {
            System.out.println(a.getName());
            System.out.println("Before Interest: " + a.getBalance());
            a.addInterest();
            System.out.println("After Interest: " + a.getBalance());
        }
    }
}