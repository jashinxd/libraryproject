# Library Project - Jason Shin
* Library Class
    * Library has two HashMaps to keep track of Library Items and Users
    * Methods for adding, removing, having users borrow and return LibraryItems if they are borrowable/returnable
    * PrintItems prints the item id, name, and borrowed
    
* User Class
    * Has fields for basic user information
    * Contains a HashMap to keep track of items that the user borrowed
    * Borrow and Remove items method add/delete items to/from HashMap