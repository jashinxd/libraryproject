import java.util.HashMap;
import java.util.Map;

public class Library {

    private Map<Integer, LibraryItem> items;
    private Map<Integer, User> users;

    public Library() {
        items = new HashMap<>();
        users = new HashMap<>();
    }

    public boolean addItem(int id, LibraryItem li) {
        items.put(id, li);
        return true;
    }

    public boolean removeItem(int id) {
        items.remove(id);
        return true;
    }

    public boolean addUser(int id, User u) {
        users.put(id, u);
        return true;
    }

    public boolean removeUser(int id) {
        users.remove(id);
        return true;
    }

    public boolean borrowItem(int itemid, int userid) {
        LibraryItem li = items.get(itemid);
        if (li instanceof Borrowable) {
            if (!li.isBorrowed()) {
                ((Borrowable) li).borrowItem();
                User user = users.get(userid);
                user.borrowItem(li);
                li.borrowItem();
                return true;
            }
        }
        System.out.println("Item not borrowed successfully.");
        return false;
    }

    public boolean returnItem(int itemid, int userid) {
        User user = users.get(userid);
        if (user.getBorrowedItem(itemid) != null) {
            ((Borrowable)items.get(itemid)).returnItem();
            user.returnItem(itemid);
            items.get(itemid).returnItem();
            return true;
        }
        System.out.println("Item not returned successfully.");
        return false;
    }

    public void printItems() {
        for (int id : items.keySet()) {
            LibraryItem li = items.get(id);
            System.out.println("" + li.getId() + "\t" + li.getTitle() + "\t" + li.isBorrowed());
        }
    }
}
