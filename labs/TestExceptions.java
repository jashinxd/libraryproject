public class TestExceptions {
    public static void main(String[] args) {
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Fingers", "Worf", "Troy", "Data"};
        Account2[] arrayOfAccounts = new Account2[5];
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            try {
                arrayOfAccounts[i] = new Account2(names[i], amounts[i], Currency.USD);
            } catch (DodgyNameException e) {
                System.out.println(e.toString());
                return;
            } finally {
                if (arrayOfAccounts[i] != null) {
                    double tax = arrayOfAccounts[i].getBalance() * 0.4;
                    arrayOfAccounts[i].setBalance(arrayOfAccounts[i].getBalance() * 0.6);
                    System.out.println(tax);
                } else {
                    System.out.println("finally runs, account not set");
                }
            }
        }
    }
}