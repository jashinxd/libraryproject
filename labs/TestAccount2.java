public class TestAccount2 {
    public static void main(String[] args) {
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        Account[] arrayOfAccounts = new Account[5];
        for (int i = 0; i < amounts.length; i++) {
            arrayOfAccounts[i] = new Account(names[i], amounts[i]);
        }
        Account.setInterestRate(0.05);
        for (Account a : arrayOfAccounts) {
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New Balance: " + a.getBalance());
        }
        for (Account a : arrayOfAccounts) {
            System.out.println("Withdrawing 20");
            System.out.println("Withdraw Status: " + a.withdraw());
            System.out.println("Amount left: " + a.getBalance());
            double withdrawAmount = 200;
            a.withdraw(withdrawAmount);
            System.out.println("Withdrawing " + withdrawAmount);
            System.out.println("Withdraw Status: " + a.withdraw());
            System.out.println("Amount left: " + a.getBalance());
        }

    }
}