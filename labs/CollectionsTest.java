import java.util.TreeSet;
import java.util.Iterator;

public class CollectionsTest {
    public static void main(String[] args) {
        TreeSet<Account> accounts;
        accounts = new TreeSet<Account>(( Account o1, Account o2) ->  {
            Double a1 = ((Account)o1).getBalance();
            Double a2 = ((Account)o2).getBalance();
            return a1.compareTo(a2);
        });
        SavingsAccount sa1 = new SavingsAccount("SA1", 1000);
        SavingsAccount sa2 = new SavingsAccount("SA2", 2000);
        CurrentAccount ca = new CurrentAccount("CA", 100);
        accounts.add(sa1);
        accounts.add(sa2);
        accounts.add(ca);
        Iterator iter = accounts.iterator();
        while (iter.hasNext()) {
            Account a = (Account)iter.next();
            System.out.println("Name: " + a.getName());
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New Balance: " + a.getBalance());
        }

        for (Account a : accounts) {
            System.out.println("Name: " + a.getName());
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New Balance: " + a.getBalance());
        }

        accounts.forEach(a -> {
            System.out.println("Name: " + a.getName());
            System.out.println("Balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New Balance: " + a.getBalance());
        }
        );

    }
}