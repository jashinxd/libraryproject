public class ThreadTest {
    public static void main(String[] args) {
        // Runnable run = () -> {
        //     for (int i = 0; i < 3; i++) {
        //         System.out.println("My message");
        //     }
        // };

        Runnable run = new MyRunnable();
        Thread thread1 = new Thread(run);
        thread1.start();
        Thread thread2 = new Thread(run);
        thread2.start();
        Thread thread3 = new Thread(run);
        thread3.start();
    }
}