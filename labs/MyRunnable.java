import java.lang.Thread;

public class MyRunnable implements Runnable {

    private String message = "hello from the thread";

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            try {
                slowMessage();
                System.out.println();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public void slowMessage() throws InterruptedException {
        synchronized (message) {
            for (char c : message.toCharArray()) {
                System.out.print(c);
                Thread.sleep(10);
            }
        }   
    }

}