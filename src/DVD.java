public class DVD extends LibraryItem implements Borrowable {

    private String publisher;
    private String genre;
    private String targetAudience;
    private boolean borrowed;

    public DVD(int id, String title, String releaseDate, String publisher, String genre, String targetAudience) {
        super.setTitle(title);
        super.setReleaseDate(releaseDate);
        super.setId(id);
        this.publisher = publisher;
        this.genre = genre;
        this.targetAudience = targetAudience;
    }

    @Override
    public boolean borrowItem() {
        borrowed = true;
        return true;
    }

    @Override
    public boolean returnItem() {
        borrowed = false;
        return true;
    }

    @Override
    public boolean isBorrowed() {
        return this.borrowed;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTargetAudience() {
        return targetAudience;
    }

    public void setTargetAudience(String targetAudience) {
        this.targetAudience = targetAudience;
    }
}
