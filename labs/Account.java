public abstract class Account implements Detailable {
    
    private double balance;
    private String name;
    private static double interestRate;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this.name = "Jason";
        this.balance = 50; 
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public abstract void addInterest();

    public String getDetails() {
        return "" + name + " " + balance + " " + interestRate;
    }

    public boolean withdraw (double amount) {
        double remaining = this.balance - amount;
        if (remaining >= 0.0) {
            this.balance = remaining;
            return true;
        }
        return false;
    }

    public boolean withdraw() {
        return withdraw(20.0);
    }
}