public class Periodical extends LibraryItem {

    private String author;
    private String publication;

    public Periodical(int id, String title, String releaseDate, String author, String publication) {
        super.setTitle(title);
        super.setReleaseDate(releaseDate);
        super.setId(id);
        this.author = author;
        this.publication = publication;
    }

    @Override
    public boolean borrowItem() {
        return false;
    }

    @Override
    public boolean returnItem() {
        return false;
    }

    @Override
    public boolean isBorrowed() {
        return false;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

}
