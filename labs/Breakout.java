public class Breakout {
    
    public static void main(String[] args) {
        // Breakout 1
        String make = "BMW", model ="530D";
        Double engineSize = 3.0;
        byte gear = 2;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        short speed = (byte)(gear * 20); 
        System.out.println("The speed is " + speed);
        
        // Breakout 2

        if (engineSize <= 1.3) {
            System.out.println("The car is a weak car");
        } else {
            System.out.println("The car is a powerful car");
        }

        if (gear == 5) {
            System.out.println("speed should be over 45 mph");
        } else if (gear == 4) {
            System.out.println("speed should be between 30 and 45 mph");
        } else if (gear == 3) {
            System.out.println("speed should be between 20 and 30 mph");
        } else if (gear == 2) {
            System.out.println("speed should be between 10 and 20 mph");
        } else if (gear == 1) {
            System.out.println("speed should be under 10mph");
        }

        for (int i = 1900; i <= 2000; i+=4) {
            System.out.println(i);
        }

        for (int i = 1900; i <= 2000; i+=4) {
            System.out.println(i);
            if (i >= 1916) {
                System.out.println("finished");
                break;
            }
        }

        switch (gear) {
            case 5:
                System.out.println("speed should be over 45 mph");
                break;
            case 4:
                System.out.println("speed should be between 30 and 45 mph");
                break;
            case 3:
                System.out.println("speed should be between 20 and 30 mph");
                break;
            case 2:
                System.out.println("speed should be between 10 and 20 mph");
                break;
            case 1:
                System.out.println("speed should be under 10mph");
                break;
        }
    }
}