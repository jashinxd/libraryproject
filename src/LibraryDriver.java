public class LibraryDriver {
    public static void main(String[] args) {
        Library l = new Library();

        // Adding LibraryItems
        Book b1 = new Book(1,"The Lord of the Rings", "29 July 1954", "J.R.R. Tolkein", "Allen & Unwin", "Fantasy", 111.11);
        CD cd1 = new CD(2, "Songs About Jane", "25 June 2002", "Maroon 5", "Matt Wallace", "Pop Rock", "46:06", "Octone");
        DVD dvd1 = new DVD(3, "The Lion King", "1994", "N/A", "Animation", "PG");
        Periodical p1 = new Periodical(4, "Smaple Periodical", "Sample Date", "Sample Authoer", "IEEE");
        User u1 = new User(1, "Jason Shin", "shin.jason@gmail.com", "06 June 1998" );

        // Adding items to library
        l.addItem(b1.getId(), b1);
        l.addItem(cd1.getId(), cd1);
        l.addItem(dvd1.getId(), dvd1);
        l.addItem(p1.getId(), p1);

        // Adding user to library
        l.addUser(u1.getId(), u1);

        // All items in library
        l.printItems();

        // User with id = 1 borrows book with id = 1 from library
        l.borrowItem(1,1);
        l.printItems();

        // User with id = 1 returns book with id = 1 from library
        l.returnItem(1, 1);
        l.printItems();

        l.removeItem(1);
        l.printItems();

        l.borrowItem(1, 1);
    }
}
