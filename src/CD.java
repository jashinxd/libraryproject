import java.time.Duration;

public class CD extends LibraryItem implements Borrowable {

    private String artist;
    private String producer;
    private String genre;
    private String duration;
    private String label;
    private boolean borrowed;

    public CD(int id, String title, String releaseDate, String artist, String producer, String genre, String duration, String label) {
        super.setTitle(title);
        super.setReleaseDate(releaseDate);
        super.setId(id);
        this.artist = artist;
        this.producer = producer;
        this.genre = genre;
        this.duration = duration;
        this.label = label;
        this.borrowed = false;
    }

    @Override
    public boolean borrowItem() {
        borrowed = true;
        return true;
    }

    @Override
    public boolean returnItem() {
        borrowed = false;
        return true;
    }

    @Override
    public boolean isBorrowed() {
        return this.borrowed;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
