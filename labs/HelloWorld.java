public class HelloWorld {

    // CTRL-Space for easy completion

    //main CTRL-SPACE Enter
    public static void main(String[] args) {
        // sysou CTRL-SPACE Enter
        System.out.println("Hello World");

        // Stick a 0 in front, octal
        // Stick a 0b in front, binary
        int myInt = 8;

        long ccard = 12341;

        float myfloat = (float)1.14324; 

        // Unicode characters can be specified in 4 digits
        char c = 'c';

        // Uses of Javadoc

        byte b1 = 120, b2 = 120; ;
        // Result of an addition is cast to int, therefore cast to byte
        byte b3 = (byte)(b1 + b2);

        // This is okay because the cast happens for you
        b1 += b2;

        System.out.println(b1);
        
        Car car = new Car();
        car.setMake("Tesla");
        car.setModel("Model S");

        System.out.println(car.getMake());
        
        // Java Beans
        //     Getters and Setters
        //     No arg constructor
        //     Serializeable


    }

}