public class TestAcount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setBalance(100);
        myAccount.setName("Jason Shin");
        System.out.println("Name: " + myAccount.getName());
        System.out.println("Balance: " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("Balance: " + myAccount.getBalance());
    }
}