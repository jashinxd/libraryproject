public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] detailableArray = new Detailable[3];
        detailableArray[0] = new CurrentAccount("Current", 200);
        detailableArray[1] = new SavingsAccount("Savings", 200);
        detailableArray[2] = new HomeInsurance(100, 2, 20);
        for (Detailable d : detailableArray) {
            System.out.println(d.getDetails());
        }
    }
}