import java.util.Comparator;

public class AccountComparator implements Comparator {
    
    @Override
    public int compare(Object o1, Object o2) {
        Double a1 = ((Account)o1).getBalance();
        Double a2 = ((Account)o2).getBalance();
        return a1.compareTo(a2);
    }
}